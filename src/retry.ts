
export function retry<TReturn>(maxRetryCount: number, func: () => Promise<TReturn> ): Promise<TReturn> {
    return new Promise<TReturn>( (resolve, reject) => doRetry(resolve, reject, 0, maxRetryCount, func));
}

function doRetry<TReturn>(resolve: (value?: any) => void, reject: (reason?: any) => void, retryCount: number, maxRetryCount: number, func: () => Promise<TReturn> ): void {
    func()
        .then(returnValue => {
            resolve(returnValue);
            },
            error => {
                if (retryCount < maxRetryCount) {
                    sleep(getRetryDuration(retryCount));
                    console.log(`retrying: ${retryCount}`);
                    doRetry(resolve, reject, ++retryCount, maxRetryCount, func);
                } else {
                    reject(error);
                }
            });
}

function getRetryDuration(retryCount: number): number {
    return Math.pow(2, retryCount) * 10;
}

function sleep(interval: number): Promise<void> {
    return new Promise(resolve => setTimeout(() => {
        resolve();
    }, interval));
}
