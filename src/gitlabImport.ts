import * as request from "request-promise-native";
import * as fs from "fs";
import { retry } from "./retry";

export class GitLabImport {
    private GitLabUrl: string;
    private GitLabToken: string;
    private GitLabProject: string;

    public constructor(gitLabUrl: string, gitLabToken: string, gitLabProject: string) {
        this.GitLabUrl = gitLabUrl;
        this.GitLabToken = gitLabToken;
        this.GitLabProject = gitLabProject;
    }

    public async createIssue(issue: IGitLabIssue): Promise<number> {
        const newIssue: IGitLabIssueResult = await retry(5, async () => await request({
            method: "post",
            url: `${this.GitLabUrl}/api/v4/projects/${encodeURIComponent(this.GitLabProject)}/issues`,
            // the GitLab issue that we have just created
            body: issue,
            json: true,
            headers: {
              "PRIVATE-TOKEN": this.GitLabToken
            }
          }));
        return newIssue.iid;
    }

    public async AddIssueComment(note: IGitLabComment, issueIid: number): Promise<void> {
       await retry(5, async () => await request({
            method: "post",
            url: `${this.GitLabUrl}/api/v4/projects/${encodeURIComponent(this.GitLabProject)}/issues/${issueIid}/notes`,
            // the GitLab issue that we have just created
            body: note,
            json: true,
            headers: {
              "PRIVATE-TOKEN": this.GitLabToken
            }
          }));
    }

    public async UploadAttachments(attachments: IGitLabAttachment[], issueIid: number): Promise<void> {
        const markdownList: string[] = [];
        for (const attachmentId in attachments) {
            if (attachments.hasOwnProperty(attachmentId)) {
                const attachmentResult = await this.UploadSingleAttachment(attachments[attachmentId]);
                markdownList.push(attachmentResult.markdown);
            }
        }
        const newComment = {
            body: `Attachments from jira:
            ${markdownList.join("\r\n")}`
        };
        if (markdownList.length > 0) {
            await this.AddIssueComment(newComment, issueIid);
        }
    }

    private sleep(interval: number): Promise<void> {
        return new Promise(resolve => setTimeout(() => {
            resolve();
        }, interval));
    }
    public async UploadSingleAttachment(attachment: IGitLabAttachment): Promise<IAttachmentResult> {
        const attachmentResult: IAttachmentResult = await retry(5, async () => await request({
            method: "post",
            url: `${this.GitLabUrl}/api/v4/projects/${encodeURIComponent(this.GitLabProject)}/uploads`,
            // the GitLab issue that we have just created
            formData: {file: {value: fs.createReadStream(`uploads/${attachment.attachmentPath}`), options: {filename: attachment.filename}}},
            json: true,
            headers: {
              "PRIVATE-TOKEN": this.GitLabToken
            }
        }));
        return attachmentResult;
    }

    public async CloseIssue(issueIid: number): Promise<void> {
        await retry(5, async () => await request({
            method: "put",
            url: `${this.GitLabUrl}/api/v4/projects/${encodeURIComponent(this.GitLabProject)}/issues/${issueIid}`,
            // the GitLab issue that we have just created
            body: {state_event: "close"},
            json: true,
            headers: {
              "PRIVATE-TOKEN": this.GitLabToken
            }
          }));
    }

    public async GetAllMergeRequests(projectName: string): Promise<IGitLabMergeRequest[]> {
        let allRequests: IGitLabMergeRequest[] = [];
        const filterExpression = "merged&scope=all&view=simple&per_page=100";
        for (let page = 1; page < 5; page++) {
            const partialRequests: IGitLabMergeRequest[] = await retry(5, async () => await request({
                method: "Get",
                url:
                `${this.GitLabUrl}/api/v4/projects/${encodeURIComponent(this.GitLabProject)}/merge_requests?state=${filterExpression}&page=${page}&search=${projectName}-`,
                // the GitLab issue that we have just created
                json: true,
                headers: {
                  "PRIVATE-TOKEN": this.GitLabToken
                }
              }));
            allRequests = allRequests.concat(partialRequests);
            if (partialRequests.length < 100) {
                break;
            }
        }
        return allRequests;
    }

}
interface IGitLabIssueResult {
    iid: number;
}
export interface IGitLabMergeRequest {
    iid: number;
    title: string;
    description: string;
}

export interface IGitLabAttachment {
    attachmentPath: any;
    filename: string;
}

interface IAttachmentResult {
    alt: string;
    url: string;
    markdown: string;
}

export interface IGitLabUser {
    name: string;
    id: number;
}

export interface IGitLabComment {
    body: string;
    created_at?: string;
}

export interface IGitLabIssue {
    title: string;
    description: string;
    assignee_ids: number[];
    labels: string;
    created_at: string;
}
